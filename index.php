<!DOCTYPE html>
<html>
<head>
	<title>Php Activity</title>
	<link rel="stylesheet" type="text/css" href="https://bootswatch.com/4/sketchy/bootstrap.css">
</head>
<body>

	<h1 class="text-center my-5">WELCOME GUEST!</h1>
		<div class="col-lg-4 offset-lg-4" >
			<form class="bg-light p-4" action="assets/controllers/process.php" method="POST">
				<div class="form-group">
					<label for="name">Name:</label>
					<input type="text" name="name" class="form-control">
				</div>
				<div class="form-group">
					<label for="birthMonth">Birth Month:</label>
					<input type="number" name="birthMonth" class="form-control">
				</div>
				<div class="form-group">
					<label for="birthDate">Birth date:</label>
					<input type="number" name="birthDate" class="form-control">
				</div>
				<div class="text-center">
					<button class="btn btn-success" type="submit">Check Zodiac</button>
				</div>
			</form>

		</div>

</body>
</html>